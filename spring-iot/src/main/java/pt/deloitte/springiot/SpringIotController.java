package pt.deloitte.springiot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SpringIotController {
    
    @Autowired
    private SpringIotProfile profile;
    
    @Autowired
    private SpringIotService service;

    @RequestMapping(value="/")
    public String index(Model m){
        m.addAttribute("name", service.getName());
        m.addAttribute("version", service.getVersion());
        m.addAttribute("description", service.getDescription());
        m.addAttribute("profile", profile.getActiveProfile());
        m.addAttribute("dbName", getDbName(service.getDbName()));

        return "index";
    }

    private String getDbName(String dbName){
        return dbName.split(";")[0].split("/")[1];
    }
}
