package pt.deloitte.springiot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class SpringIotProfile {

    @Autowired
    private Environment env;

    public String getActiveProfile(){
        String[] profile = env.getActiveProfiles();

        if(profile.length == 0){
            return env.getDefaultProfiles()[0];
        }else{
            return profile[0];
        }
    }
}
