package pt.deloitte.springiot.simulator;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.annotations.Expose;

import lombok.ToString;

@Service
@ToString
public class Simulator implements ISimulator{

    @Autowired
    private SimulatorComponent setup;

    @Expose
    private boolean blink = false;
    @Expose
    private final AtomicInteger counter = new AtomicInteger();
    @Expose
    private float random;

    private long lastBlinkTime;
    
    public Simulator(){
        this.lastBlinkTime = System.currentTimeMillis();
    } 

    @Override
    public String getBlink() {
        long time = System.currentTimeMillis();
        long diff = time - this.lastBlinkTime;
        if(diff>setup.getBlinkMillis()){
            this.blink = !blink;
            this.lastBlinkTime = time;
        }

        return String.valueOf(blink);
    }

    @Override
    public String getCounter() {
        return String.valueOf(counter.getAndIncrement());
    }

    @Override
    public String getRandom() {
       this.random = Math.round(Math.random() * setup.getMaxRandom());
       return String.valueOf(this.random);
    }

    @Override
    public void refresh() {
        this.getBlink();
        this.getCounter();
        this.getRandom();
    }
    
    public static void main(String[] args) {
        ISimulator sim = new Simulator();

        System.out.println(sim);
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(sim);
    }
}
