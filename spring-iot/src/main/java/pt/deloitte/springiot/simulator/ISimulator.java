package pt.deloitte.springiot.simulator;

public interface ISimulator {
    public String getBlink();
    public String getCounter();
    public String getRandom();

    public void refresh();
}
