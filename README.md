# Spring IoT

* author: __João Mendes__
* date: __14/02/2024__

## Deply

### local run

__linux__

    ./mvnw spring-boot:run

__windows__

    mvnw.cmd spring-boot:run

> NOTE: `JAVA_HOME` must be declared on user variables

```sh
$ exposrt | grep JAVA_HOME declare -x JAVA=HOME"<location of jdk>"
```

### local build

__clear target build folder__

    mvnw clear

__target build__

    mvnw package

## Run

    java -jar target/<app>.jar

## Activate profiles

### With environment variables

    set spring_profiles_active=<profile>

### From maven ops 

    mvnw.cmd spring-boot:run -Dspring-boot.run.profiles=dev

### From java ops

    java -jar target/<app>.jar --spring.profiles.active=<profile>


## Testes

    mvnw.cmd test